# Pointer information

## Summary

* `c_long` is 8 bytes on:
  * macos arm64
  * macos x86_64
  * linux x86_64
* `c_long` is 4 bytes on:
  * Raspberry Pi
  * windows


## macos arm64

```
Target os: macos
Pointer width: 64-bit
size of u8: 1
size of i8: 1
size of u16: 2
size of i16: 2
size of u32: 4
size of i32: 4
size of u64: 8
size of i64: 8
size of c_int: 4
size of c_long: 8
size of c_longlong: 8
```

## Raspberry Pi 3

```
Target os: linux
Pointer width: 32-bit
size of u8: 1
size of i8: 1
size of u16: 2
size of i16: 2
size of u32: 4
size of i32: 4
size of u64: 8
size of i64: 8
size of c_int: 4
size of c_long: 4
size of c_longlong: 8
```

## x86_64 linux

```
Target os: linux
Pointer width: 64-bit
size of u8: 1
size of i8: 1
size of u16: 2
size of i16: 2
size of u32: 4
size of i32: 4
size of u64: 8
size of i64: 8
size of c_int: 4
size of c_long: 8
size of c_longlong: 8
size of f32: 4
size of f64: 8
size of c_float: 4
size of c_double: 8
```

## x86_64 Windows msys2

```
Target os: windows
Pointer width: 64-bit
size of u8: 1
size of i8: 1
size of u16: 2
size of i16: 2
size of u32: 4
size of i32: 4
size of u64: 8
size of i64: 8
size of c_int: 4
size of c_long: 4
size of c_longlong: 8
size of f32: 4
size of f64: 8
size of c_float: 4
size of c_double: 8
```

## x86_64 macos

```
Target os: macos
Pointer width: 64-bit
size of u8: 1
size of i8: 1
size of u16: 2
size of i16: 2
size of u32: 4
size of i32: 4
size of u64: 8
size of i64: 8
size of c_int: 4
size of c_long: 8
size of c_longlong: 8
```
