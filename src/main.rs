fn type_sizes<T>(name: &str) {
    println!("size of {}: {}", name, std::mem::size_of::<T>());
}

fn pointer_width() {
    print!("Pointer width: ");
    if cfg!(target_pointer_width = "16") {
        println!("16-bit");
    } else if cfg!(target_pointer_width = "32") {
        println!("32-bit");
    } else if cfg!(target_pointer_width = "64") {
        println!("64-bit");
    } else {
        println!("???");
    }
}

fn target_os() {
    print!("Target os: ");
    if cfg!(target_os = "windows") {
        println!("windows");
    } else if cfg!(target_os = "macos") {
        println!("macos");
    } else if cfg!(target_os = "linux") {
        println!("linux");
    } else {
        println!("???");
    }
}

fn main() {
    target_os();
    pointer_width();
    // integer types
    type_sizes::<u8>("u8");
    type_sizes::<i8>("i8");
    type_sizes::<u16>("u16");
    type_sizes::<i16>("i16");
    type_sizes::<u32>("u32");
    type_sizes::<i32>("i32");
    type_sizes::<u64>("u64");
    type_sizes::<i64>("i64");
    type_sizes::<std::os::raw::c_int>("c_int");
    type_sizes::<std::os::raw::c_long>("c_long");
    type_sizes::<std::os::raw::c_longlong>("c_longlong");
    // floating point types
    type_sizes::<f32>("f32");
    type_sizes::<f64>("f64");
    type_sizes::<std::os::raw::c_float>("c_float");
    type_sizes::<std::os::raw::c_double>("c_double");
}
